
>`For SSH`: **one time process 🦥**
>---
- create ssh key type: `ssh-keygen -t ed25519 -C "<comment>"`
- key is saved in `$HOME/.ssh/`
- folder contains a `private key`, and a `pub key`
- copy `pub key` and return to gitlab 
- click `preferences`in profile icon 🐦‍⬛	
- open SSH tab 
- paste pub key
- save key

>`Steps`: **edit config files 💅**
>---
1. download `myconfig` repository using `git clone`
2. go the `myconfig` folder
3. edit files as necessary
4. git add `.`
5. git commit -m "message to add"
6. git push 

> `Misc Packages : `
Exa 					-extended core utils
Powerlevel10k - oh my ZSH
Omf						- oh my fish

