#!/bin/bash

sudo apt update 
sudo apt upgrade 

#dconf editor is used to access the config files of Gnome
sudo apt-get -y install dconf-editor
dconf dump / > $HOME/myconfig/gnome/svk.rc

#This will restore all setting to default
dconf reset -f /

#Load your custom setting
dconf dump / < $HOME/myconfig/gnome/profile1.rc

