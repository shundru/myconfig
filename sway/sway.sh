#!/bin/bash

#Create soft Link for config
#create softlink for config.d folder
#create softlink for vartiables in variable.d folder
#

if [[ -e $HOME/.config/sway/config ]]; then
    cp $HOME/.config/sway/config $HOME/.config/sway/config.bakup
    ln -sf $HOME/myconfig/sway/config  $HOME/.config/sway/config
    ln -sf $HOME/myconfig/sway/config.d/*  $HOME/.config/sway/config.d
    ln -sf $HOME/myconfig/sway/variables.d/* $HOME/.config/sway/variables.d
    ln -sf $HOME/myconfig/sway/outputs $HOME/.config/sway/outputs
else
    ln -sf $HOME/myconfig/sway/config  $HOME/.config/sway/config 
    ln -sf $HOME/myconfig/sway/config.d/*  $HOME/.config/sway/config.d
    ln -sf $HOME/myconfig/sway/variables.d/* $HOME/.config/sway/variables.d/
    ln -sf $HOME/myconfig/sway/outputs $HOME/.config/sway/outputs
fi

