if status is-interactive
    # Commands to run in interactive sessions can go here
set -x EDITOR nvim
set -x PAGER less
set -x VISUAL nvim
#All aliases come here
alias gd="git diff"
alias gs="git status"
alias code="cd ~/Developer/code"
alias dev="cd ~/Developer/"
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias ..='cd ..'
alias nb='newsboat'
alias b='newsboat'
alias boatrc='nvim ~/.newsboat/urls'
alias bashrc='nvim ~/.bashrc'
alias profilerc='nvim ~/.profile'
alias update='sudo apt update' 
alias upgrade='sudo apt upgrade'
alias v='nvim'
alias vim='nvim'
alias neo='neofetch'
alias r='ranger'
alias n='nvim'
alias myconfig='cd ~/myconfig/'
alias myscript='cd ~/myconfig/myscript'
alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'
alias grep='grep --color=auto'
alias neo='neofetch'
alias btop='btop --utf-force'
alias mv='mv -i'
alias cp='cp -i'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias rm='rm -i'
alias ..='cd ..'
alias nb='newsboat'
alias bashrc='nvim ~/.bashrc'
alias basha="nvim ~/myconfig/.bash/.bash_aliases"
alias grep='grep --color=auto'
alias boatrc='nvim ~/.newsboat/urls'
alias update='sudo apt update && sudo apt upgrade'
alias fishrc="nvim ~/.config/fish/config.fish"
alias myconfig='cd $HOME/myconfig/'
alias matrix='cmatrix'
alias config="cd ~/.config/"
alias con="cd ~/.config/"
alias boat="cd ~/myconfig/.newsboat"    
alias ls="eza --long --header --inode --icons"
alias la="eza --long --tree --level=2 --icons"
alias ff="fastfetch"
alias sendit='git add . && git commit -m "git update " && git push origin main'


end
