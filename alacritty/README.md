<h1>Alacritty can be installed from "git" using the below link</h1>

If you are reading this then perhaps you want to install alacritty
Sometime "sudo apt install alacritty" does not work. Then you have to build it from source
The alacritty.toml file can be used to set up your terminal.

<h5>Remember to install cargo, c++ complier (g++ or clang++)<h5>

> _Read this link _ 

[https://github.com/alacritty/alacritty/blob/master/README.md](https://github.com/alacritty/alacritty/blob/master/README.md)	

