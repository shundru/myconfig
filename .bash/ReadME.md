
# **Structure && Role**

### First file to load:`/etc/profile` 
This is a root file. Don't touch it !


### Second to load :`.bash_profile`

> File functions to source other **important files** like _~/.bashrc_ or _~/.profile_  from appropriate locations <br>
> For e.g. env variables are sourced from ~/.profiles

### The third to load :`.bashrc`

> Here you should keep color tab completion, aliases,themes etc. <br> 
> Can include alias but I save in seperate file

### The fouth to load:`.profile`

> Here we keep the `env variables`.<br>
> This is how bash would know where to look for `binary`

### The fifth to load :`.bash_aliases`

> Contains all alaises

