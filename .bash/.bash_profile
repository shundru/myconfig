# Source .bashrc 

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	  . "$HOME/.bashrc" # the dot equivalent to source == "."
    fi
fi

# Source .profile

if [-f ~/.profile]; then
    source ~/.profile # source == "."
fi

#Source Bash Aliases
if [ -f ~/.bash_aliases ]; then
. ~/.bash-aliases
fi

