#!/bin/bash

# If vim is installed then copy vimRC else do nothing
#
if [[ -e $HOME/.vimrc ]]; then
    mv $HOME/.config/nvim/init.vim $HOME/.config/nvim/init.bak
    ln -sf $HOME/myconfig/.vimrc $HOME/
else
    echo " NO ACTION TAKEN >>> NO VIM FOUND"
fi


