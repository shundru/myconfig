#!/bin/bash


sudo apt update && sudo apt upgrade
sudo apt install alacritty
mkdir -p $HOME/.config/alacritty/

#Create soft Link to files
ln -sf $HOME/myconfig/alacritty/alacritty.toml $HOME/.config/alacritty/

# Pull latest theme from git

mkdir -p ~/.config/alacritty/themes
git clone https://github.com/alacritty/alacritty-theme ~/.config/alacritty/themes



