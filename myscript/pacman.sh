#!/bin/bash

sudo pacman -Syyu

#Directory Creations
mkdir -p $HOME/.config/nvim
mkdir -p $HOME/.config/fish
mkdir -p $HOME/.local/share/fonts
mkdir -p $HOME/.newsboat
mkdir -p $HOME/.config/kitty/
mkdir -p $HOME/.config/nano/
touch $HOME/.bash_profile
touch $HOME/.bash_aliases

#Installs
sudo pacman -Syu firefox fish neovim micro ranger \
	cmatrix neofetch newsboat timeshift kitty \
	transmission-gtk btop exa

#Installing font
sudo pacman -S ttf-roboto-mono
#Installing Multiple fonts
sudo pacman -S ttf-dejavu ttf-liberation noto-fonts


#Backup folders before symlink
cp $HOME/.config/nvim/init.vim $HOME/.config/nvim/init.vim.bakup
cp $HOME/.config/kitty/kitty.conf  $HOME/.config/kitty/kitty.conf.bakup
cp $HOME/.config/fish/config.fish  $HOME/.config/fish/config.fish.backup


#Create soft Link to files

ln -sf $HOME/myconfig/.bash/.bash_aliases $HOME/.bash_aliases
ln -sf $HOME/myconfig/.bash/.bash_profile $HOME/.bash_profile
ln -sf $HOME/myconfig/nvim/init.vim $HOME/.config/nvim/
ln -sf $HOME/myconfig/.newsboat/urls $HOME/.newsboat/   
ln -sf $HOME/myconfig/.newsboat/config $HOME/.newsboat/	
ln -sf $HOME/myconfig/fonts/* $HOME/.local/share/fonts/ 
ln -sf $HOME/myconfig/nano/nanorc $HOME/.config/nano/
ln -sf $HOME/myconfig/kitty/kitty.conf $HOME/.config/kitty/
#ln -sf $HOME/myconfig/.vimrc $HOME/


#Optional Installs
#cargo
#export PATH=$HOME/.cargo/bin:$PATH >> $HOME/.bashrc 

# If  bashrc exist then source alias from .bash_aliases

if [[ -e $HOME/.bashrc ]]; then
    echo "source $HOME/.bash_aliases" >> $HOME/.bashrc
else
    echo "source $HOME/myconfig/.bash/.bashrc" > $HOME/.bashrc
fi

# If fish exist then append to fishrc

if [[ -e $HOME/.config/fish/config.fish ]]; then
    echo "source $HOME/myconfig/fish/config.fish" >> $HOME/.config/fish/config.fish
else
    ln -sf $HOME/myconfig/fish/config.fish $HOME/.config/fish/       
fi
