# Installs Debian and Ubuntu Related scripts
#!/bin/bash

sudo apt update && sudo apt upgrade

#Directory Creations
mkdir -p $HOME/.config/nvim
mkdir -p $HOME/.local/share/fonts
mkdir -p $HOME/.newsboat
mkdir -p $HOME/.config/nano/
touch $HOME/.bash_aliases


# Adding bash profile if it doesn't exist
if [ -e "$HOME/.bash_profile" ] || [ -e "$HOME/.profile" ]; then
    echo "No action is necessary since .bash_profile or .profile already exist"
else
    touch "$HOME/.bash_profile"
    ln -s "$HOME/myconfig/.bash/.bash_profile" "$HOME/.bash_profile"
fi

#Essential packages
sudo apt install neovim
sudo apt install curl
sudo apt install ranger 
sudo apt install cmatrix 
#sudo apt install eza 
sudo apt install newsboat 
sudo apt install gcc clang 
sudo apt install fastfetch 
sudo apt install fish

#Microsoft font and updates cache
sudo apt-get install ttf-mscorefonts-installer
sudo apt-get install fontconfig
sudo fc-cache -f -v


#Softlink for fonts repo
ln -sf $HOME/myconfig/fonts/* $HOME/.local/share/fonts/ 
sudo ln -sf $HOME/myconfig/fonts/* /usr/share/fonts/ 

#Soft Link to config files
 
ln -sf $HOME/myconfig/.bash/.bash_aliases $HOME/.bash_aliases
ln -sf $HOME/myconfig/.bash/.bash_ssh $HOME/.bash_ssh
ln -sf $HOME/myconfig/nvim/init.vim $HOME/.config/nvim/
ln -sf $HOME/myconfig/.newsboat/urls $HOME/.newsboat/   
ln -sf $HOME/myconfig/.newsboat/config $HOME/.newsboat/	
ln -sf $HOME/myconfig/nano/nanorc $HOME/.config/nano/

#Optional Installs

#cargo
#export PATH=$HOME/.cargo/bin:$PATH >> $HOME/.bashrc 

# If  bashrc exist then source alias from .bash_aliases

if [[ -e $HOME/.bashrc ]]; then
    echo "source $HOME/.bash_aliases" >> $HOME/.bashrc
    echo "source $HOME/.bash_ssh" >> $HOME/.bashrc
else
    touch $HOME/.bashrc
    ln -sf $HOME/myconfig/.bash/.bashrc $HOME/.bashrc
    echo "source $HOME/myconfig/.bash/.bashrc" > $HOME/.bashrc
    echo "source $HOME/myconfig/.bash/.bash_ssh" >> $HOME/.bashrc
fi

sudo apt autoremove
