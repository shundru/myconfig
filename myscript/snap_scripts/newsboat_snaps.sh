#!/bin/bash

# Check if config file exits
# Create softlink for all newsboat config files into snapd
#

if [[ -e $HOME/snap/newsboat/7707/.newsboat  ]]; then
    ln -sf  ~/myconfig/.newsboat/* ~/snap/newsboat/7707/.newsboat/
else
    mkdir -p $HOME/snap/newsboat/7707/.newsboat
    ln -sf  ~/myconfig/.newsboat/* ~/snap/newsboat/7707/.newsboat/
fi

