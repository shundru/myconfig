#!/bin/bash

# The following script will install Oh my fish plugin on FISH Terminal

curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish 

echo "Type omf install + <tab> choose <theme name e.g. lambda> "
omf install lambda
