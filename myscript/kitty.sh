#!/bin/bash
#

if [[ -f $HOME/.config/kitty/kitty.conf ]]; then
    mv $HOME/.config/kitty/kitty.conf $HOME/.config/kitty/kitty.backup
    ln -sf $HOME/myconfig/kitty/kitty.conf $HOME/.config/kitty/
    echo "Backup created before linking kitty config file"
else
    ln -sf $HOME/myconfig/kitty/kitty.conf $HOME/.config/kitty/
    echo " Kitty config was just copied no back neccessary"
fi


#
